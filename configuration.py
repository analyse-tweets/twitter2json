import json


class Configuration:
    def __init__(self, config_file):
        with open(config_file) as f:
            config = json.load(f)

        self.key = config['key']
        self.secret_key = config['secret_key']
        self.token = config['token']
        self.secret_token = config['secret_token']
        self.keywords = config['keywords']
# Dates au format année-mois-jour
        self.start_date = config['start_date']
        self.end_date = config['end_date']
        self.tweets_ids_output_file = config['tweets_ids_output_file']
        self.max_tweets_day = config['max_tweets_day']
# Un nom parmi 'Firefox', 'Chrome', 'Safari' et 'PhantomJS'
        self.webdriver_name = config['webdriver_name']
