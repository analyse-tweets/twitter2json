#! /usr/bin/python3
from selenium import webdriver
from configuration import Configuration
from selenium.common.exceptions import NoSuchElementException,\
    StaleElementReferenceException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from time import sleep
from datetime import datetime, timedelta
import threading


def form_url(since, until, keywords):
    url = 'https://twitter.com/search?f=tweets&vertical=default&q='
    for k in keywords:
        url += k + "%20OR%20"

    url = url[:-8]  # Supprime le dernier "%20OR%20"

    url += '%20since%3A' + since.strftime("%Y-%m-%d") + \
           '%20until%3A' + until.strftime("%Y-%m-%d") + '&src=typd'
    return url


class SeekIdsTweets():
    tweet_selector = 'li.js-stream-item'
    id_selector = '.time a.tweet-timestamp'
    ids_tweets = set()
    ids_locker = threading.Lock()

    def __init__(self, driver_name):
        if driver_name.lower() == 'firefox':
            self.driver = webdriver.Firefox()
        elif driver_name.lower() == 'safari':
            self.driver = webdriver.Safari()
        elif driver_name.lower() == 'phantomjs':
            dcap = dict(DesiredCapabilities.PHANTOMJS)
            dcap["phantomjs.page.settings.userAgent"] = (
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 9_1_3) "
                    "AppleWebKit/602.3.12 (KHTML, like Gecko) "
                    "Version/10.0.2 Safari/602.3.12"
            )
            self.driver = webdriver.PhantomJS(
                    desired_capabilities=dcap,
                    service_args=['--load-images=no'])
        else:
            self.driver = webdriver.Chrome()

        self.is_used = False

    def start(self, start_date, keywords, max_tweets):
        self.is_used = True
        self.driver.get(form_url(
                        start_date,
                        start_date + timedelta(days=1),
                        keywords))

        print('Starting seeking the {:%d, %b %Y}'.
              format(start_date))
        nb_tweets_day = 0
        last_nb_tweets = 0
        nb_same_nb = 0

        while (nb_tweets_day <= max_tweets and
                nb_same_nb <= 10):

            last_nb_tweets = nb_tweets_day
            found_tweets = []

            try:
                found_tweets = self.driver.\
                    find_elements_by_css_selector(self.tweet_selector)
            except NoSuchElementException:
                print('No tweets on this day.')

            nb_tweets_day = len(found_tweets)

            sleep(1)
            self.driver.execute_script('window.scrollTo(0,'
                                       'document.body.scrollHeight);')

            if last_nb_tweets == nb_tweets_day:
                nb_same_nb += 1
            else:
                nb_same_nb = 0

        for tweet in found_tweets:
            try:
                id_tweet = tweet.\
                            find_element_by_css_selector(self.id_selector).\
                            get_attribute('href').split('/')[-1]

                SeekIdsTweets.ids_locker.acquire()
                SeekIdsTweets.ids_tweets.add(id_tweet)
                SeekIdsTweets.ids_locker.release()
            except StaleElementReferenceException as e:
                print('Lost element reference.', tweet)

        print('\n{} tweets found the {:%d, %b %Y}; {} total'.
              format(nb_tweets_day,
                     start_date,
                     len(SeekIdsTweets.ids_tweets)))
        self.is_used = False

    def start_thread(self, start_date, keywords, max_tweets):
        self.is_used = True
        threading.Thread(target=self.start,
                         args=(start_date,
                               keywords,
                               max_tweets)
                         ).start()

    def quit_driver(self):
        if self.is_used:
            return False
        else:
            self.driver.quit()
            return True


def get_ids_tweets(
        start_date,
        end_date,
        max_tweets_day,
        keywords,
        driver_name='chrome',
        nb_threads=1):
    """ Cherche les ids des tweets entre les dates 'start_date' et 'end_date'
    avec les mots clés 'keywords' et le navigateur 'driver_name'.
    Le nombre de tweets récupéré par jour est limité à max_tweets_day.

        - 'start_date' et 'end_date' sont des chaines de caractères au format
        'AAAA-MM-JJ'
        - 'keywords' est une liste de chaines de caractères
        - 'driver_name' doit contenir 'firefox', 'chrome', 'safari' ou
        'phantomjs'
    """

# === Initialisations ===

# --- Dates ---
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    days = (end_date - start_date).days + 1

# --- Drivers (accès au navigateur) ---
    seekers = list()
    for i in range(nb_threads):
        seekers.append(SeekIdsTweets(driver_name))

# === Exécution de la requête pour chaque jour ===
    for day in range(days):
        seeking = False
        while not seeking:
            for s in seekers:
                if not s.is_used:
                    s.start_thread(start_date, keywords, max_tweets_day)
                    seeking = True
                    break
            sleep(1)

        start_date += timedelta(days=1)

# === Fermetures ===
    while len(seekers) > 0:
        for s in seekers:
            if s.quit_driver():
                seekers.remove(s)
        sleep(1)

    print('Research done, {} tweets found'.
          format(len(SeekIdsTweets.ids_tweets)))
    return SeekIdsTweets.ids_tweets


if __name__ == '__main__':
    config = Configuration("config.json")

    ids_tweets = get_ids_tweets(
            config.start_date,
            config.end_date,
            config.max_tweets_day,
            config.keywords,
            config.webdriver_name,
            4)
    with open(config.tweets_ids_output_file, 'w') as outfile:
        for id in ids_tweets:
            outfile.write(id + '\n')

    print('All done here')
